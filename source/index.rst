################################
Dobrodošli na reStructuredHgWiki
################################

HgWiki nas je vjerno služio u protekle 3 godine i uvelike nam olakšao pripremu materijala za vježbe, ali je došlo vrijeme za promjenu.

Cilj nam je od početka reStructuredHgWiki učiniti čišćim nego što je to HgWiki. Pod time mislimo staviti fokus isključivo na sadržaj i smanjiti količinu distrakcije. U ostvarenju tog cilja su nam uvelike pomogli sveprisutni `Bootstrap <http://getbootstrap.com/>`__ i vizualno dojmljivi `Bootswatch <http://bootswatch.com/>`__ (tema `Cerulean <http://bootswatch.com/cerulean/>`__). Sadržaj `HgWikija <http://inf2.uniri.hr/hgwiki/>`__ pretvoren je u `reStructuredText <http://en.wikipedia.org/wiki/ReStructuredText>`__, a `Sphinx <http://sphinx-doc.org/>`__ koristimo za prevođenje sadržaja u HTML, PDF i ostale formate. Sadržaj se kontinuirano nadopunjava i osvježava, a datum zadnje promjene uvijek je naveden u naslovnoj traci.

U vrijeme dok smo koristili HgWiki postojao je značajan interes od strane studenata za poboljšanjem podrške za mobilne uređaje. Svjesni smo da je popularnost mobilnih uređaja u današnje vrijeme velika, i da oni nude brojne prednosti pred klasičnim računalima. Kako bi studentima omogućili da uče kad žele i gdje žele, reStructuredHgWiki je od samog početka potpuno `responzivan <http://en.wikipedia.org/wiki/Responsive_web_design>`__ te ga je moguće koristiti i na mobilnim uređajima i njihovim web preglednicima.

Sugestije za poboljšanje sadržaja su dobrodošle, međutim, još su više dobrodošli patchevi izvornog koda materijala. Izvorni kod i upute su dostupni putem `službenog repozitorija <http://bitbucket.org/infuniri/restructuredhgwiki>`__  na korisničkom računu Odjela za informatiku na `Bitbucketu <http://bitbucket.org/>`__. Pogreške u sadržaju su moguće, naročito u ovako ranom stadiju, i naročito stilske pogreške i tipfeleri; molimo da uočene pogreške prijavite `putem Bitbucketa <http://bitbucket.org/infuniri/restructuredhgwiki/issues/new>`__.

***************************************************
Kolegiji Odjela za informatiku Sveučilišta u Rijeci
***************************************************

.. toctree::
   :maxdepth: 2

   kolegiji/OI1
   kolegiji/OS1
   kolegiji/OS2
   kolegiji/PPHS
   kolegiji/DS
   kolegiji/RM1
   kolegiji/RM2
   kolegiji/UMS

***************************************
Kolegiji ostalih sastavnica Sveučilišta
***************************************

.. toctree::
   :maxdepth: 2

   kolegiji/RM-RiTeh
   kolegiji/NRM-RiTeh
   kolegiji/KINF
   kolegiji/DBAMRM

************************
Radionice na Sveučilištu
************************

.. toctree::
   :maxdepth: 2

   kolegiji/PPHS-UniRi

*****************
Indeksi i tablice
*****************

* :ref:`genindex`
* :ref:`search`

