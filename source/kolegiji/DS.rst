#####################
Distribuirani sustavi
#####################

******
Vježbe
******

Rad u programskom jeziku Python
===============================

.. toctree::
   :maxdepth: 2

   ../materijali/python-osnove-sintakse
   ../materijali/python-input-output
   ../materijali/python-funkcije-klase
   ../materijali/python-modularizacija
   ../materijali/python-testiranje
   ../materijali/python-dokumentiranje
   ../materijali/python-timing-benchmarking

Pomoćni Python moduli i alati
=============================

.. toctree::
   :maxdepth: 2

   ../materijali/git-upravljanje-verzijama
   ../materijali/python-modul-numpy
   ../materijali/python-modul-scipy
   ../materijali/python-modul-matplotlib
   ../materijali/python-okvir-waf

Izvođenje procesa na distribuiranim sustavima korištenjem sučelja MPI
=====================================================================

.. toctree::
   :maxdepth: 2

   ../materijali/paralelno-distribuirano-heterogeno-racunarstvo-pojmovi
   ../materijali/python-modul-mpi4py-osnove
   ../materijali/python-modul-mpi4py-komunikacija-tocka-do-tocke
   ../materijali/python-modul-mpi4py-kolektivna-komunikacija
   ../materijali/python-modul-mpi4py-komunikatori-grupe
   ../materijali/python-modul-mpi4py-dinamicko-upravljanje-procesima
   ../materijali/python-modul-mpi4py-jednostrana-komunikacija
   ../materijali/python-modul-mpi4py-paralelni-ulaz-izlaz
   ../materijali/python-modul-mpi4py-upravljanje-okolinom
