####################
Osnove informatike 1
####################

******
Vježbe
******

Operacijski sustav Linux
========================

.. toctree::
   :maxdepth: 2

   ../materijali/linux-unix-povijest-osnove
   ../materijali/oi1-linux-upravljanje-datotecnim-sustavom
   ../materijali/oi1-linux-upravljanje-datotecnim-sustavom-2

Regularni izrazi i Perl
=======================

.. toctree::
   :maxdepth: 2

   ../materijali/oi1-regularni-izrazi-konacni-automati
   ../materijali/oi1-perl
