#####################
Operacijski sustavi 1
#####################

******
Vježbe
******

Osnove rada u komandnolinijskom sučelju
=======================================

.. toctree::
   :maxdepth: 2

   ../materijali/linux-unix-povijest-osnove
   ../materijali/linux-cli-uvod
   ../materijali/linux-direktoriji-mkdir-pwd-cd
   ../materijali/linux-datoteke-touch-rm-cp-mv
   ../materijali/emacs-text-datoteke
   ../materijali/grep-sed-awk-tr
   ../materijali/linux-cijevi-preusmjeravanje-ulaza-izlaza
   ../materijali/linux-arhiviranje-komprimiranje

Napredniji rad u komandnolinijskom sučelju
==========================================

.. toctree::
   :maxdepth: 2

   ../materijali/linux-shell-varijable-povijest
   ../materijali/linux-procesi
   ../materijali/linux-disk-particija-filesystem
   ../materijali/linux-inode-poveznice
   ../materijali/linux-dozvole-datoteka
   ../materijali/linux-tipovi-datoteka
