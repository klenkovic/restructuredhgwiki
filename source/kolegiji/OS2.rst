#####################
Operacijski sustavi 2
#####################

******
Vježbe
******

Rad u programskom jeziku Python
===============================

.. toctree::
   :maxdepth: 2

   ../materijali/python-osnove-sintakse
   ../materijali/python-input-output
   ../materijali/python-funkcije-klase
   ../materijali/python-modularizacija
   ../materijali/python-standardna-biblioteka
   ../materijali/python-modul-sys

Rad s programskim sučeljima operacijskog sustava
================================================

.. toctree::
   :maxdepth: 2

   ../materijali/strace-ltrace-sucelja-operacijskog-sustava
   ../materijali/python-modul-time
   ../materijali/python-timing-benchmarking
   ../materijali/python-modul-os
   ../materijali/python-modul-subprocess
   ../materijali/python-modul-signal
   ../materijali/python-modul-socket
   ../materijali/python-modul-select
   ../materijali/python-modul-xmlrpc
   ../materijali/python-modul-locale
   ../materijali/python-modul-fcntl

Programiranje aplikacija za paralelno i distribuirano izvođenje
===============================================================

.. toctree::
   :maxdepth: 2

   ../materijali/paralelno-distribuirano-heterogeno-racunarstvo-pojmovi
   ../materijali/python-modul-threading
   ../materijali/python-modul-multiprocessing
   ../materijali/python-modul-mpi4py-osnove
   ../materijali/python-modul-mpi4py-komunikacija-tocka-do-tocke
   ../materijali/python-modul-mpi4py-kolektivna-komunikacija
