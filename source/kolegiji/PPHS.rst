################################################
Paralelno programiranje na heterogenim sustavima
################################################

 *"Make no little plans. They have no magic to stir men's blood and probably themselves will not be realized. Make big plans; aim high in hope and work, remembering that a noble, logical diagram once recorded will never die, but long after we are gone will be a living thing, asserting itself with ever-growing insistency. Remember that our sons and grandsons are going to do things that would stagger us. Let your watchword be order and your beacon beauty. Think big."*

 -- Daniel Burnham 

******
Vježbe
******

Rad u programskom jeziku Python
===============================

.. toctree::
   :maxdepth: 2

   ../materijali/python-osnove-sintakse
   ../materijali/python-input-output
   ../materijali/python-funkcije-klase
   ../materijali/python-modularizacija
   ../materijali/python-testiranje
   ../materijali/python-dokumentiranje
   ../materijali/python-timing-benchmarking

Pomoćni Python moduli i alati
=============================

.. toctree::
   :maxdepth: 2

   ../materijali/git-upravljanje-verzijama
   ../materijali/python-modul-numpy
   ../materijali/python-modul-scipy
   ../materijali/python-modul-matplotlib
   ../materijali/python-okvir-waf

Programiranje aplikacija za heterogene sustave korištenjem tehnologije CUDA
===========================================================================

.. toctree::
   :maxdepth: 2

   ../materijali/paralelno-distribuirano-heterogeno-racunarstvo-pojmovi
   ../materijali/python-modul-pycuda-osnove
   ../materijali/python-modul-pycuda-zbrajanje-vektora
   ../materijali/python-modul-pycuda-redukcija
   ../materijali/python-modul-pycuda-matrice
   ../materijali/python-modul-pycuda-vektorski-tipovi
   ../materijali/python-modul-pycuda-funkcije-uredjaja-domacina
   ../materijali/python-modul-pycuda-biblioteke-funkcija
   ../materijali/python-modul-pycuda-hijerarhija-memorije
   ../materijali/python-modul-pycuda-otklanjanje-gresaka
   ../materijali/python-modul-pycuda-profiliranje-optimizacija
   ../materijali/python-modul-pycuda-tokovi-asinkrone-operacije
   ../materijali/python-modul-pycuda-multi-gpu

Programiranje aplikacija za heterogene sustave korištenjem tehnologije OpenCL
=============================================================================

.. toctree::
   :maxdepth: 2

   ../materijali/python-modul-pyopencl
