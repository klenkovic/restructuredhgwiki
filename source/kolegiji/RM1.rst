#################
Računalne mreže 1
#################

.. todo::

  Nedostaju uvodne informacije o kolegiju.

*********************
Laboratorijske vježbe
*********************

.. todo::

  Nedostaju uvodne informacije o labosima.

Laboratorijska vježba 1
=======================

.. toctree::
   :maxdepth: 2

   ../materijali/emulator-core-graficko-sucelje
   ../materijali/ping-traceroute-nestat-analiza-mreze

Laboratorijska vježba 2
=======================

.. toctree::
   :maxdepth: 2

   ../materijali/wireshark-snimanje-prometa
   ../materijali/wireshark-filtriranje-paketa

Laboratorijska vježba 3
=======================

.. toctree::
   :maxdepth: 2

   ../materijali/quagga-usmjeravanje
   ../materijali/ifconfig-konfiguracija-mreznih-sucelja
   ../materijali/emulator-core-konfiguracija-cvora

******************
Dodatni materijali
******************

.. toctree::
   :maxdepth: 2

   ../materijali/emulator-core-subnetiranje
   ../materijali/dhcp-posluzitelj-klijent
   ../materijali/click-programska-osnova-usmjerivaca
