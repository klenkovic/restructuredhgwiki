#############################
Upravljanje mrežnim sustavima
#############################

.. todo::

  Nedostaju uvodne informacije o kolegiju.

******
Vježbe
******

Ponavljanje gradiva
===================

.. toctree::
   :maxdepth: 2
   
.. toctree::
   :maxdepth: 2

   ../materijali/linux-unix-povijest-osnove
   ../materijali/linux-cli-uvod
   ../materijali/linux-direktoriji-mkdir-pwd-cd
   ../materijali/linux-datoteke-touch-rm-cp-mv
   ../materijali/emacs-text-datoteke
   ../materijali/grep-sed-awk-tr
   ../materijali/linux-cijevi-preusmjeravanje-ulaza-izlaza
   ../materijali/linux-arhiviranje-komprimiranje
   ../materijali/linux-shell-varijable-povijest
   ../materijali/linux-procesi
   ../materijali/linux-disk-particija-filesystem
   ../materijali/linux-inode-poveznice
   ../materijali/linux-dozvole-datoteka
   ../materijali/linux-tipovi-datoteka

Upravljanje operacijskim sustavom
=================================

.. toctree::
   :maxdepth: 2

   ../materijali/linux-korisnici-grupe
   ../materijali/yum-rpm-upravljanje-paketima-operacijskog-sustava
   ../materijali/grub-pokretanje-sustava
   ../materijali/systemd-systemctl-upravljanje-uslugama-operacijskog-sustava
   ../materijali/systemd-journalctl-pracenje-dogadjaja-operacijskog-sustava
   ../materijali/systemd-cron-automatizacija-zadaca-operacijskog-sustava
   ../materijali/selinux-mandantna-kontrola-pristupa
   ../materijali/linux-kernel-osnove
   ../materijali/procfs-sysfs-informacije-konfiguracija
   ../materijali/iptables-vatrozid
   ../materijali/iptables-prevodjenje-adresa

Upravljanje aplikacijama na sustavu
===================================

.. toctree::
   :maxdepth: 2

   ../materijali/gcc-gxx-make-prevodjenje-programa
   ../materijali/nabavka-prevodjenje-instalacija-softvera-otvorenog-koda
   ../materijali/api-abi-kompatibilnost

Upravljanje mrežnim uslugama
============================

.. toctree::
   :maxdepth: 2

   ../materijali/openssh-kljucevi-autentikacija
   ../materijali/apache-web-posluzitelj
   ../materijali/mariadb-sustav-za-upravljanje-bazom-podataka
   ../materijali/apache-wodpress-mediawiki-instalacija-web-aplikacija
   ../materijali/postfix-procmail-smtp-posluzitelj
   ../materijali/dovecot-pop3-imap-posluzitelj
   ../materijali/spamassasin-spam-filter
   ../materijali/bind9-dns-sustav
   ../materijali/avahi-zeroconf

Tips and tricks
===============

.. toctree::
   :maxdepth: 2

   ../materijali/screen-tmux-multipleksiranje-terminala
