.. sectionauthor:: `Vedran Miletić <http://vedranmileti.ch/>`__

Konfiguracija web poslužitelja Apache HTTP Server
=================================================

.. hint::

  Za više informacija proučite `Chapter 9. Web Servers <https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/System_Administrators_Guide/ch-Web_Servers.html>`__ u `Red Hat Enterprise Linux 7 System Administrator's Guide <https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/System_Administrators_Guide/>`__.

* HTTP(S) poslužitelj, inicijalno zasnovan na poslužitelju `NCSA HTTPd <https://en.wikipedia.org/wiki/NCSA_HTTPd>`__
* najkorišteniji web poslužitelj prema `Netcraft April 2014 Web Server Survey <http://news.netcraft.com/archives/2014/04/02/april-2014-web-server-survey.html>`__, slijede `Microsoft Internet Information Services <https://en.wikipedia.org/wiki/Internet_Information_Services>`__ i `nginx <https://en.wikipedia.org/wiki/Nginx>`__
* aktualna verzija je 2.4 i ima vrlo detaljnu `službenu dokumentaciju <http://httpd.apache.org/docs/2.4/>`__

Osnovna konfiguracija
---------------------

* ``ServerRoot``
* ``DocumentRoot``
* ``Listen``

.. admonition:: Zadatak

  Učinite da poslužitelj sluša samo na localhost adresi, odnosno ``127.0.0.1``. Možete li mu pristupiti izvana?

* ``UserDir``

.. admonition:: Zadatak

  Uključite podršku za ``UserDir`` i direktorij postavite na ``public_html``. Unutar kućnog direktorija korisnika ``vmiletic`` stvorite direktorij ``public_html``, a unutar njega datoteku ``stranica.html`` s proizvoljnim sadržajem. Učitajte adresu http://<IP adresa poslužitelja>/~vmiletic/stranica.html da bi provjerili radi li ``UserDir`` kako treba.

Modul mod_php
-------------

* ``LoadModule``

.. admonition:: Zadatak

  * Instalirajte PHP (paket ``php``).
  * Provjerite je li se mijenjao sadržaj datoteke ``/etc/httpd/conf/httpd.conf``.
  * Provjerite je li se mijenjao sadržaj direktorija ``/etc/httpd/conf.d`` i ``/etc/httpd/modules``.
  * Unutar ``DocumentRoot``-a stvorite datoteku ``index.php`` koja ispisuje ``phpinfo()`` na ekran.

* ``ServerName``
* ``ServerAdmin``
* ``UseCanonicalName``
* ``ServerSignature``

.. admonition:: Zadatak

  * Promijenite ime poslužitelja u infuniri-minion<broj>.uniri.hr:80.
  * Promijenite mail adresu administratora u infuniri-minion<broj>-admin@uniri.hr.
  * Uključite korištenje kanonskog imena servera.
  * Provjerite razliku u ispisu između ``On``, ``Off`` i ``EMail`` vrijednosti kod ``ServerSignature`` direktive.

Modul mod_ssl
-------------

* modul za SSL i TLS podršku (omogućuje pristup web serveru putem HTTPS protokola)
* paket se zove ``mod_ssl``
* generiranje certifikata i konfiguracija servera opisana je u `9.1.7. Setting Up an SSL Server <https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/System_Administrators_Guide/ch-Web_Servers.html#s2-apache-mod_ssl>`__

Modul mod_wsgi
--------------

* modul za Web Server Gateway Interface (WSGI), najpopularniji način korištenja Python web aplikacija
* paket se zove ``mod_wsgi``
* jednostavna WSGI aplikacija ima kod oblika

  .. code-block:: python

    def application(environ, start_response):
        status = '200 OK'
        output = 'Ovo je demo WSGI aplikacija.'

        response_headers = [('Content-type', 'text/plain'),
                            ('Content-Length', str(len(output)))]
        start_response(status, response_headers)

        return [output]

* uključivanje WSGI skripte u web server

  .. code-block:: apacheconf

    WSGIScriptAlias /myapp /usr/local/www/wsgi-scripts/myapp.wsgi

* konfiguracija dozvola

  .. code-block:: apacheconf

    <Directory /var/www/wsgi-scripts>
      Order allow,deny
      Allow from all
    </Directory>
