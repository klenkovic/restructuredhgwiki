.. sectionauthor:: `Vedran Miletić <http://vedranmileti.ch/>`__

Osnove rada s emulatorom računalnih mreža
=========================================

Pojam emulacije i emulatora
---------------------------

U proučavanju računalnih mreža koriste se dva osnovna tipa alata, tzv. **emulatori** i **simulatori**. Postoji više načina da se definira razlika između ta dva tipa alata; mi ćemo tu razliku napraviti na temelju *vremena*, i reći da emulatori rade u realnom vremenu, a simulatori u vlastitom (simuliranom) vremenu. Dakle, ako emulator treba emulirati 120 sekundi rada mreže i aplikacija, ta će emulacija zaista trajati 120 sekundi stvarnog vremena, dok simulatora simulacija 120 sekundi rada mreže može trajati mnogo više ili mnogo manje od 120 sekundi stvarnog vremena.

U ovoj vježbi i naredne dvije koristit ćemo emulator CORE.

Osnovne informacije o alatu CORE
--------------------------------

Alat **Common Open Research Emulator** (CORE) je `emulator računalnih mreža <http://en.wikipedia.org/wiki/Network_emulation>`__. CORE izrađuje prikaz stvarne računalne mreže i omogućuje korisniku rad s njom u stvarnom vremenu. Takva emulacija, obzirom da radi u stvarnom vremenu, može se povezati i sa stvarnim mrežama, a u njoj se mogu koristiti i stvarne mrežne aplikacije. CORE podržava Linux i FreeBSD, a na Linuxu radi na osnovi kontejnerske virtualizacije `LinuX Containers (LXC) <http://en.wikipedia.org/wiki/LXC>`__.

Alat CORE je razvijen od strane istraživačkog odjela `Boeinga <http://www.boeing.com/>`__ uz podršku `istraživačkog laboratorija Američke mornarice <http://www.nrl.navy.mil/>`__, a zasnovan je na alatu `Integrated Multiprotocol Network Emulator/Simulator (IMUNES) <http://www.imunes.tel.fer.hr/>`__ razvijenom na `Zavodu za telekomunikacije <http://www.fer.unizg.hr/ztel>`__ `Fakulteta elektrotehnike i računarstva Sveučilišta u Zagrebu <https://www.fer.unizg.hr/>`__.

CORE ima `vrlo bogatu dokumentaciju <http://downloads.pf.itd.nrl.navy.mil/docs/core/core-html/>`__ koja je dostupna na `službenim stranicama alata <http://www.nrl.navy.mil/itd/ncs/products/core>`__.

Grafičko korisničko sučelje alata CORE
--------------------------------------

CORE pokrećemo naredbom ``core-gui`` u terminalu ili stavkom ``CORE Network Emulator`` u meniju.
::

  $ core-gui

Time dobivamo prazno platno na koje možemo dodavati čvorove i veze. U izborniku s lijeve strane imamo redom

* ``selection tool``, za odabir objekata emulacije,
* ``start the session``, koji pokreće emulaciju,
* ``link tool``, alat za stvaranje veza,
* ``network-layer virtual nodes``, alat za stvaranje čvorova koji poznaju mrežni i više slojeve

  * ``router``, usmjerivač koji koristi alat Quagga za izradu tablica usmjeravanja
  * ``host``, emulirano poslužiteljsko računalo koje u zadanim postavkama ima pokrenut SSH poslužitelj,
  * ``pc``, emulirano račulano koje u zadanim postavkama nema pokrenutih procesa.

* ``link-layer nodes``, alat za stvaranje čvorova koji rade na sloju veze podataka

  * ``ethernet hub``, Ethernet koncentrator koji primljene pakete prosljeđuje svima koji su na njega povezani,
  * ``ethernet switch``, Ethernet preklopnik koji primljene pakete prosljeđuje pametnije korištenjem tablice prosljeđivanja,

* ``background annotation tools``, koji služe za estetski dojam.

Čvorove postavljamo odabirom željenog čvora iz grupe ``network-layer virtual nodes`` ili ``link-layer nodes`` i klikom na odgovarajuće mjesto. Čvorove konfiguriramo odabirom opcije ``Configure`` u izborniku dostupnom na desni klik. Veze postavljamo odabirom alata ``link tool``, klikom na jedan od dva željena čvor i povlačenjem do drugog. Veze konfiguriramo analogno čvorovima, desnim klikom i odabirom opcije ``Configure``.

U dijalogu ``link configuration`` možemo postaviti:

* ``Bandwidth``, u bitovima po sekundi,
* ``Delay``, u mikrosekundama,
* ``PER``, odnosno postotak paketa koji zadobiju greške u prijenosu,
* ``Duplicate``, odnosno postotak paketa koji su duplicirani u prijenosu,
* ``Color``, odnosno boju crte kojom je veza nacrtana na platnu (što je osobito značajno za estetski dojam),
* ``Width``, odnosno širinu crte kojom je veza nacrtana na platnu (što je također vrlo značajno za estetski dojam).

Nakon slaganja željene mreže emulaciju pokrećemo klikom na gumb ``start the session``, koji zatim postaje ``stop the session``.

Dok je emulacija pokrenuta, ostali alati u izborniku su:

* ``observer widgets tool``, koji omogućuje odabir značajki koje će se prikazivati kod prijelaza preko čvora pokazivačem miša,
* ``plot tool``, koji omogućuje da lijevim klikom na vezu nacrtamo graf propusnosti; graf se miče desnim klikom,
* ``marker``, koji služi za vizualno naglašavanje pojedinih djelova emulacije kod prezentacija,
* ``two-node tool``, koji omogućuje pokretanje naredbi ``ping`` i ``traceroute`` na dva čvora; čvorovi se biraju klikom na pravokutnik pored ``source node``, odnosno ``destination node`` i zatim na odgovarajući čvor.
* ``run tool``, koji omogućuje pokretanje proizvoljnih naredbi na jednom ili više čvorova.

Za vrijeme dok je emulacija pokrenuta moguće je pristupiti ljusci svakog pojedinog čvora desnim klikom na čvor i odabirom opcije ``Shell window/bash``.

`Službena dokumentacija grafičkog korisničkog sučelja alata CORE <http://downloads.pf.itd.nrl.navy.mil/docs/core/core-html/usage.html>`__ ima više detalja o opisanoj funkcionalnosti i ostalim mogućnostima grafičkog sučelja.

Spremanje i učitavanje emulacijskih scenarija
---------------------------------------------

Scenarije za emulaciju koji smo složili moguće je spremiti kao ``.imn`` datoteku korištenjem ``File/Save as...`` i kasnije učitati korištenjem ``File/Open...``. Gotovi scenariji dostupni su u direktoriju ``.core/configs`` unutar kućnog direktorija korisnika.

Da bi ilustrirali dostupne gotove scenarije, naredbom
::

  $ core-gui .core/configs/sample2-ssh.imn

otvorit ćemo jedan od jednostavnijih primjera. S druge strane, jedan od atraktivnijih primjera koji uključuje mobilne čvorove čije kretanje je prikazano na platnu u stvarnom vremenu, možemo otvoriti naredbom
::

  $ core-gui .core/configs/sample1.imn

Pokretanje emulacije vrši se kao i u situaciji kada slažemo vlastitu emulaciju.

.. caution::

  CORE omogućuje istovremeno pokretanje više sesija emulacije. U slučaju da vam to počne stvarati probleme (imate zaostale sesije emulacije koje ne možete uništiti korištnjem gumba ``Shutdown`` ili sl.), uvijek možete izvršiti ponovno pokretanje virtualne mašine, što će osigurati čišćenje svih pokrenutih sesija i riješiti problem.

Emulacija mrežnog prometa korištenjem alata MGEN
------------------------------------------------

CORE omogućuje korištenje alata Multi-Generator (MGEN) za generiranje prometa. MGEN također razvijen od strane istraživačkog laboratorija Američke mornarice, a moguće ga je koristiti i neovisno o alatu CORE. Mi se tim načinom korištenja ovdje nećemo baviti; za više informacija proučite `službenu dokumentaciju <http://downloads.pf.itd.nrl.navy.mil/docs/mgen/mgen.html>`__ dostupnu na `službenim stranicama alata MGEN <http://www.nrl.navy.mil/itd/ncs/products/mgen>`__.

Odabirom opcije ``Tools/Traffic`` otvara se dijalog ``CORE traffic flows`` u kojem je moguće klikom na gumb ``new`` definirati novi tok paketa. Odabir izvornog i odredišnog čvora vrši se klikom na pravokutnik pored ``source node``, odnosno ``destination node`` i zatim na odgovarajući čvor. Pored toga moguće je konfigurirati:

* ``port`` za izvor i odredište,
* ``protocol``, TCP ili UDP,
* ``pattern``, odnosno pravilo po kojem će se paketi slati.

U dijelu ``Traffic options`` moguće je postaviti način na koji se pokreću podatkovni tokovi:

* kod opcije ``Do not start traffic flows automatically`` očekuje se da korisnik nakon pokretanja emulacije pokrene tokove ručno opcijom ``Start all flows`` ili ``Start selected`` tok po tok, dok
* kod opcije ``Start traffic flows after all nodes have booted`` postavlja automatsko pokretanje svih tokova nakon što se pokrenu svi čvorovi.

.. caution::

  Preporuča se stvaranje i konfiguriranje tokova nakon pokretanja emulacije te korištenje varijante ``Do not start traffic flows automatically``. Tokove možete ručno pokrenuti nekoliko desetaka sekundi nakon pokretanja same emulacije.
