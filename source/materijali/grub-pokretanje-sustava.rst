.. sectionauthor:: `Vedran Miletić <http://vedranmileti.ch/>`__

Boot učitavač GRUB
==================

* `GNU GRUB <http://en.wikipedia.org/wiki/GNU_GRUB>`__ je `boot učitavač <http://en.wikipedia.org/wiki/Booting#Modern_boot_loaders>`__ (engl. *bootloader*)
 
  * pokreće se prije pokretanja samog operacijskog sustava i nudi korisniku mogućnost izbora koji operacijski sustav želi pokrenuti
  * primjerice, kod Debiana GRUB nudi normalan način rada, način rada za oporavak operacijskog sustava (engl. *recovery*), testiranje radne memorije računala i pokretanje preostalih operacijskih sustava na računalu

* `initrd <http://en.wikipedia.org/wiki/Initrd>`__ i `initramfs <http://en.wikipedia.org/wiki/Initramfs>`__

  * sadrži module jezgre koji se koriste za dosezanje particije na kojoj se nalazi operacijski sustav
  * npr. ``pata_atiixp``, ``pata_amd``, ``sata_nv``, ``ahci``; ``raid0``, ``raid1``; ``ext3``, ``ext4``, ``btrfs``
