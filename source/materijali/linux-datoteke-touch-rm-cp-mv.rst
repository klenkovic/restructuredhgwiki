.. sectionauthor:: `Vedran Miletić <http://vedranmileti.ch/>`__, Vanja Slavuj, Sanja Pavkov

Baratanje datotekama u datotečnom sustavu
=========================================

Stvaranje, kopiranje, brisanje i preimenovanje datoteka
-------------------------------------------------------

* ``touch`` stvara praznu datoteku danog imena
* ``rm`` briše datoteku danog imena
* ``cp`` kopira datoteku danog imena u datoteku drugog danog imena; **primjer:**
  ::

    $ cp datoteka1 datoteka2 # kopira datoteke (prvu u drugu)

* ``mv`` preimenuje (miče) datoteku danog imena; **primjer:**
  ::

    $ mv datoteka1 datoteka2 # premješta datoteku1 u datoteku2 i preimenuje ih

.. admonition:: Zadatak

  * U svom kućnom direktoriju napravite direktorij ``zadatak1510`` i uđite u njega.
  * U njemu napravite 3 datoteke: ``vjezba1``, ``vjezba2`` i ``vjezba3`` jednom naredbom (linijom). Ispišite na ekran sadržaj datoteke ``vjezba2``.
  * Izbrišite zatim datoteku ``vjezba1``.
  * U istom direktoriju napravite i direktorij ``dir1``. 
  * Kopirajte datoteku ``vjezba3`` u ``dir1``, jednom koristeći reltivno a drugi put apsolutno referenciranje.

* ``cp -r`` i ``rm -r`` označava rekurzivno kopiranje i brisanje, briše direktorij i sve datoteke i poddirektorije u njemu

  * kod ``mv ``naredbe, za razliku od ``cp`` i ``rm``, nema potrebe za rekurzijom; radi se o preimenovanju

.. admonition:: Zadatak

  * U Vašem kućnom direktoriju napravite predloženu strukturu direktorija:
    ::

      studentXY ------ Studij --------- Preddiplomski -------- DINP.txt
                                 |                        |
                                 |----- Pravilnik.txt     |--- Raspored.txt

  * Kopirajte datoteku ``Pravilnik.txt`` u direktorij ``Preddiplomski``.
  * Kopirajte sav sadržaj direktorija ``Preddiplomski``, u direktorij ``Diplomski``, koristeći apsolutno referenciranje.
  * Izbrišite direktorij ``Preddiplomski``.
  * Direktorij ``Diplomski`` preimenujte u ``Dipl``.

.. admonition:: Dodatni zadatak

  Provjerite čemu služi i kako se koristi program `Midnight Commander <https://en.wikipedia.org/wiki/Midnight_Commander>`__ (naredba ``mc``).

Glob uzorci
-----------

* mnogo radimo sa nazivima datoteka pa postoji mogućnost rada sa posebnim znakovima (koji nemaju doslovno značenje) da bismo brzo i lako specificirali nazive većeg broja datoteka
* `glob uzorci <https://en.wikipedia.org/wiki/Glob_(programming)>`__, globovi ili wildcards

  * koriste se za pretraživanje uzoraka koji odgovaraju zadanome
  * način da više datoteka koje imaju slična imena povežemo jednom naredbom
  * glob != regex, samo ima donekle sličnu sintaksu i namjenu

* ``?`` -- jedan znak, bilo koji
* ``*`` -- bilo koliko bilo kojih znakova
* ``[chars]`` -- jedan znak, bilo koji od navedenih u zagradama, može i raspon oblika ``[A-Z]``, ``[a-z]`` ili ``[0-9]``
* ```<:klasa:>`__`` -- zamjenjuje samo jedan, bilo koji znak koji je član navedene klase

  * najčešće korištene klase su: ``[:alnum:]``, ``[:alpha:]``, ``[:digit:]``, ``[:lower:]``, ``[:upper:]``

* ``\`` -- tzv. *prekidni znak*

.. admonition:: Zadatak

  * U svom kućnom direktoriju stvorite poddirektorij ``Zadatci`` i  u njemu datoteke ``zadatak``, ``zadatek``, ``zadatuk``, ``zadatak1``, ``zadatak2``, ``zadatakABC``, ``zadatakabc``, ``zadacnica``, ``zadacnicA``, ``zad3`` i ``dat05``.
  * Jednom naredbom, koristeći se glob-om, izlistajte samo:

    * ``zadatak``, ``zadatek``, ``zadatuk``;
    * ``zadatek``, ``zadatuk``;
    * samo datoteke koje na 8 mjestu naziva imaju veliko slovo
    * samo datoteke koje počinju slovom ``z``, na 5 mjestu naziva im nije ni malo ni veliko slovo koja se po abecedi nalazi nakon slova ``s``, i čiji naziv završava malim slovom
    * sve datoteke čiji naziv završava brojem manjim od 4
    * sve navedene datoteke

  * Isprobajte naredbu ``ls [^ad]*`` i razmislite o njezinom značenju.
  * Isprobajte naredbu ``ls {ab,dat,f}??`` i razmislite o njezinom značenju.
  * Isprobajte naredbu ``cat *`<:upper:]1-4]``. Što ona radi?

Pretraživanje datotečnog sustava
--------------------------------

* ``locate`` pretražuje bazu datoteka za datoteku koja u imenu sadrži dani niz znakova
* ``find`` u specificiranim direktorijima traži datoteke ili skupine datoteka

  * sintaksa: ``find direktoriji uvjeti``

.. admonition:: Zadatak

  Napišite naredbu ``find`` kojom u svom kućnom direktoriju tražite datoteke koje počinju sa ``iz-``, čiji ste vi vlasnik i kojima je pristupano u zadnjih 30 dana. (**Uputa:** koristite pomoć ``man`` stranice naredbe ``find``.)

.. admonition:: Ponovimo!

  * Prisjetite se naredbi za stvaranje, brisanje i kopiranje datoteka. Koja naredba može primiti više argumenata?
  * Koja se naredba koristi za pomicanje neke datoteke u datotečnom sustavu? Koja za preimenovanje? Objasnite.
  * Što su globovi?
  * Koja je razlika između znakova ``?`` i ``*`` kod globova?
