.. sectionauthor:: `Vedran Miletić <http://vedranmileti.ch/>`__

Korisnici i grupe
=================

* naredba ``useradd``
* naredba ``usermod``
* naredba ``userdel``
* naredba ``groupadd``
* naredba ``groupmod``
* naredba ``groupdel``

Promjena vlasnika i grupe
-------------------------

* naredba ``chown``
* naredba ``chgrp``

Sposobnosti datoteka
--------------------

* engl. *file capabilities*
* naredba ``setcap``
* naredba ``getcap``
