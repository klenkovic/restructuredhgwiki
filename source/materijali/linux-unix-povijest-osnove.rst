.. sectionauthor:: `Vedran Miletić <http://vedranmileti.ch/>`__, Vanja Slavuj

Povijesni pregled razvoja Unixa i operacijskih sustava sličnih Unixu
====================================================================

Počeci Unixa, BSD, Solaris
--------------------------

* `Ken Thompson <http://en.wikipedia.org/wiki/Ken_Thompson>`__ i `Dennis Ritchie <http://en.wikipedia.org/wiki/Dennis_Ritchie>`__, `AT&T Bell Labs <http://en.wikipedia.org/wiki/Bell_Labs>`__

  * započinju razvoj operacijskog sustava `Multics <http://en.wikipedia.org/wiki/Multics>`__ 1960-tih godina
  * videoigra `Space Travel <http://en.wikipedia.org/wiki/Space_Travel_(video_game)>`__ ima problema s performansama na tom OS-u
  * motivirani time razvijaju Unics (izgovara se *eunuchs*, simbolizira kastrirani Multics) kao zamjenu za Multics, cilj je da bude bitno jednostavniji te zbog toga boljih performansi
  * Unics postaje `Unix <http://en.wikipedia.org/wiki/Unix>`__ 1970-tih godina

* Unix se dijeli u dvije grane

  * `AT&T <http://en.wikipedia.org/wiki/AT&T>`__ izdaje `UNIX System III <http://en.wikipedia.org/wiki/UNIX_System_III>`__ i `UNIX System V <http://en.wikipedia.org/wiki/UNIX_System_V>`__ 1980-tih godina
  * nastaje `Berkeley Software Distribution <http://en.wikipedia.org/wiki/Berkeley_Software_Distribution>`__ (BSD) kao modifikacija AT&T Unixa

* BSD Unix, krajem 1970-tih i tijekom 1980-tih godina

  * začetnik je `Bill Joy <http://en.wikipedia.org/wiki/Bill_Joy>`__ sa `University of California, Berkeley <http://www.berkeley.edu/>`__
  * brzo postaje popularan na sveučilištima i institutima, vrlo je važan za razvoj tehnologija na kojima se zasniva `Internet <http://en.wikipedia.org/wiki/Internet>`__
  * verzije 1BSD, 2BSD, 3BSD, 4BSD, 4.1BSD, 4.2BSD, 4.3BSD, ...

* `FreeBSD <http://en.wikipedia.org/wiki/FreeBSD>`__, nasljednik BSD Unixa, 1990-te

  * `A Narrative History of BSD <http://www.youtube.com/watch?v=ds77e3aO9nA>`__, video u kojem `Marshall Kirk McKusick <http://en.wikipedia.org/wiki/Marshall_Kirk_McKusick>`__ priča o razvoju Unixa i Interneta na Berkeleyu
  * `BSD is Dying, Jason Dixon, NYCBSDCon 2007 <http://www.youtube.com/watch?v=g7tvI6JCXD0>`__, šaljiv video na temu FreeBSD-a, parodija na `"Netcraft now confirms: \*BSD is dying <http://everything2.com/title/BSD+is+dying>`__, ima priču o povijesti Unixa od početka do doba FreeBSD-a

* `Sun Microsystems <http://en.wikipedia.org/wiki/Sun_Microsystems>`__, danas Oracle, također ima svoju verziju Unixa

  * `SunOS <http://en.wikipedia.org/wiki/SunOS>`__, tijekom 1980-tih i početka 1990-tih godina, zasnovan na BSD Unixu
  * `Solaris <http://en.wikipedia.org/wiki/Solaris_(operating_system)>`__, od 1990-tih godina nadalje, zasnovan na već spomenutom `AT&T UNIX System V Release 4 <http://en.wikipedia.org/wiki/UNIX_System_V#SVR4>`__

GNU i Linux, otvoreni kod
-------------------------

* Unix vs Unix-like
* GNU's Not Unix (kraće GNU), započinje 1984.

  * projekt ima za cilj stvoriti slobodan operacijski sustav sličan Unixu
  * ime dolazi od toga što GNU nije zasnovan na Unixu

* Linux, započinje 1991., autor Linus Torvalds

  * *Hello everybody out there using minix -* * I'm doing a (free) operating system (just a hobby, won't be big and professional like gnu) for 386(486) AT clones. (...) * *PS. Yes -- it's free of any minix code, and it has a multi-threaded fs. It is NOT portable (uses 386 task switching etc), and it probably never will support anything other than AT-harddisks, as that's all I have :-(.*
  * Torvalds-Tanenbaum debata, 1992.

* GNU/Linux -- kombinacija Linux jezgre i GNU softvera, cjeloviti operacijski sustav sličan Unixu

  * nazivlje Linux ili GNU/Linux -- postoje zagovornici oba
  * operacijski sustavi slični Unixu prilično su različiti od Windowsa, `pa je to i Linux <http://linux.oneandoneis2.org/LNW.htm>`__

    * zbog tih razlika i brojnih mogućnosti koje nude ponekad su neprivlačni novim korisnicima
    * zbog tih razlika i brojnih mogućnosti koje nude odlična su podloga za izučavanje značajki operacijskih sustava

* Program otvorenog koda

  * izvorni kod programa dostupan je svakome tko ga želi preuzeti
  * programski kod napisan je u nekom od poznatih programskih jezika
  * nadopunjavanje i testiranje
  * svatko može pridonijeti -- razvoj interneta je to omogućio (mailing liste, forumi, i sl.); volonteri

Linux distribucije danas
------------------------

* Linux distribucija

  * skupina programa koje su spojile grupe ljudi (najčešće iz idealističkih motiva) ili tvrtke (najčešće zbog profita)
  * uključuje sve komponente koje su potrebne da bi korisnik mogao koristiti operacijski sustav
  * najviše korištene distribucije se mijenjaju vremenom, nepreciznu statistku vodi `DistroWatch Page Hit Ranking <http://distrowatch.com/stats.php?section=popularity>`__
  * deset najpopularnijih distribucija s opisom: `DistroWatch: Top Ten Distributions <http://distrowatch.com/dwres.php?resource=major>`__

* Neke od najvažnijih Linux distribucija:

  * `Ubuntu <http://www.ubuntu.com/desktop/features>`__, započeo 2004.

    * osnova joj je Debian (započeo 1993.)
    * primarno namijenjen krajnim korisnicima
    * na njoj se zasniva `Linux Mint <http://www.linuxmint.com/>`__

  * `Fedora <http://fedoraproject.org/en/features/>`__, započela 2003.

    * osnova joj je Red Hat Linux (započeo 1994.)
    * primarno namijenjena Linux entuzijastima
    * na njoj se zasnivaju `Red Hat Enterprise Linux <http://www.redhat.com/products/enterprise-linux/>`__ i `CentOS <http://centos.org/>`__

.. admonition:: Ponovimo!

  * Kada je započeo GNU projekt i koji je njegov cilj?
  * Što je Linux, a što GNU/Linux?
  * Što je program otvorenog koda?
  * Navedite barem dvije poznate Linux distribucije.
