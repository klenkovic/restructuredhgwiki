.. sectionauthor:: `Vedran Miletić <http://vedranmileti.ch/>`__

Nabavka, kompajliranje i instalacija softvera otvorenog koda
============================================================

`Softver otvorenog koda <http://en.wikipedia.org/wiki/Open-source_software>`__ (engl. *open-source software*) je softver dostupan u obliku izvornog koda kod kojeg su prava na izvorni kod dana pod licencom koja dozvoljava korisnicima proučavanje i promjenu izvornog koda, te eventualno i daljnju distribuciju

`GPL <http://en.wikipedia.org/wiki/GNU_General_Public_License>`__ i `BSD licence <http://en.wikipedia.org/wiki/BSD_licenses>`__ su najčešće korištene; predstavljaju dva moguća odgovora na pitanje o tome što znači pojam *slobodan softver*, odnosno *otvoreni kod*. Video `BSD v. GPL, Jason Dixon, NYCBSDCon 2008 <http://www.youtube.com/watch?v=mMmbjJI5su0>`__ daje pregled razlika.

Osim GPL-a i BSD licenci, ostoje i `brojne druge licence <http://en.wikipedia.org/wiki/Comparison_of_free_software_licenses>`__.

Nabavka softvera
----------------

Neki izvori slobodnog softvera su: `GNU Software <http://www.gnu.org/software/>`__, `GitHub <http://github.com/>`__, `Bitbucket <http://bitbucket.org/>`__, `Gitorious <http://gitorious.org/>`__, `SourceForge <http://sourceforge.net/>`__ i `Google Code <http://code.google.com/hosting/>`__

Preuzimanje softvera podrazumijeva preuzimanje arhiva, za što se mogu koristiti `Wget <http://en.wikipedia.org/wiki/Wget>`__, `cURL <http://en.wikipedia.org/wiki/CURL>`__ i drugi alati za dohvaćanje sadržaja sa poslužitelja na webu putem HTTP-a, HTTPS-a i FTP-a.

Kompajliranje i instalacija softvera
------------------------------------

* ``./configure`` je skripta ljuske (``#!/bin/sh``) koja provjerava postoje li potrebne bibliotečne datoteke i prilagođava postavke kompajliranja našem sustavu i našim željama
* ``make`` vrši kompajliranje izvornog koda softvera u izvršni kod
* ``make install`` vrši instalaciju

  * najčešće se instalacija u zadanim postavkama vrši u ``/usr/local``, kako bi se ručno instalirani softver odvojio od onoga instaliranog pomoću upravitelja paketima koji ide u ``/usr`` (potrebno je imati dozvolu zapisivanja u taj direktorij -- u većini slučajeva to znači biti ``root``)

.. admonition:: Zadatak

  * Na `službenim stranicama uređivača teksta GNU nano <http://nano-editor.org/>`__ preuzmite arhivu s izvornim kodom.
  * Raspakirajte arhivu koju ste preuzeli, a zatim izvršite konfiguraciju i kompajliranje.
  * Pokušajte izvršiti instalaciju, da uočite što točno ne uspijeva.
  * Pokušajte pronaći ``nano`` unutar direktorija gdje ste izvršili kompajliranje i pokrenite ga direktno. (**Napomena:** Takav način pokretanja kompajliranih programa neće uvijek raditi, ali za GNU nano specijalno hoće.)

* ``./configure --prefix=<path>`` omogućuje da se putanja ``<path>`` koristi kao mjesto za instalaciju umjesto predefinirane

.. admonition:: Zadatak

  * Na `službenim stranicama VLC media playera <http://www.videolan.org/>`__ preuzmite arhivu s izvornim kodom.
  * Raspakirajte je i pokušajte izvršiti konfiguraciju. Objasnite što skripta za konfiguraciju ne nalazi na sustavu.
  * Ono što VLC-u nedostaje moguće je instalirati u proizvoljne direktorije, ali je potrebno odgovarajućim parametrom ``--with-<biblioteka>`` navesti gdje se nalazi. Napravite to za prve dvije nedostajuće biblioteke.

.. admonition:: Dodatni zadatak

  * Na `službenim stranicama uređivača teksta GNU nano <http://nano-editor.org/>`__ moguće je GNU nano preuzeti i putem SVN-a.
  * Pronađite kako, a zatim izvršite kompajliranje po uputama koje su tamo navedene.
  * Pokrenite ``nano`` na isti način kao u prethodnom zadatku da se uvjerite da se zaista radi o različitoj verziji.
  * Istražite koju naredbu pokreće skripta ljuske ``autogen.sh``. U ``man`` stranici te naredbe proučite što rade parametri koji su navedeni u skripti i kojem skupu alata pripada navedena naredba.
