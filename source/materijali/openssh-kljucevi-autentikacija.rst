.. sectionauthor:: `Vedran Miletić <http://vedranmileti.ch/>`__

Sigurna ljuska i udaljeni rad alatom OpenSSH
============================================

Upravljanje ključevima
^^^^^^^^^^^^^^^^^^^^^^

.. todo::

  Ovdje treba opisati ``ssh-keygen`` i ``ssh-copy-id``.

Konfiguracija SSH poslužitelja
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. todo::

  Ovdje treba opisati najvažnije postavke SSH poslužitelja (``PermitRootLogin``, ``PasswordAuthentication``, ``AcceptEnv``).

Konfiguracija autentikacije
^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. todo::

  Ovdje treba objasniti kako se koriste različite vrste autentikacije (zaporke, ključevi, LDAP).
