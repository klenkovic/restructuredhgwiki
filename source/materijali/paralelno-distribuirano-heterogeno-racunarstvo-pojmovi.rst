.. sectionauthor:: `Vedran Miletić <http://vedranmileti.ch/>`__

Osnovni pojmovi paralelnog, distribuiranog i heterogenog računarstva
====================================================================

Paralelna obrada
----------------

Paralelizacija čini da se više radnji, operacija ili proračuna izvodi istovremeno. **Radnje, operacije ili proračuni moraju biti takvi da je paralelizacija moguća.** Ako to nije slučaj, vrijeme izvođenja paralelne varijante biti će jednako serijskoj, ili čak duže.

Paralelizacija se može izvesti na tri razine:

* procesi
* `procesne niti <http://en.wikipedia.org/wiki/Thread_(computer_science)>`__ (engl. *threads*)
* `koprogrami <http://en.wikipedia.org/wiki/Coroutines>`__ (engl. *coroutines*), `vlakna <http://en.wikipedia.org/wiki/Fiber_(computer_science)>`__ (engl. *fibers*) ili `zelene niti <http://en.wikipedia.org/wiki/Green_threads>`__ (engl. *green threads*)

Općenito, tri zakona govore o mogućnosti kraćenja vremena izvođenja korištenjem paralelizacije:

* `Littleov zakon <http://en.wikipedia.org/wiki/Little's_law>`__,
* `Amdahlov zakon <http://en.wikipedia.org/wiki/Amdahl's_law>`__,
* `Gustafsonov zakon <http://en.wikipedia.org/wiki/Gustafson's_law>`__.

Primjene paralelne obrade
-------------------------

Tipovi problema na koje se često primjenjuju metode paralelnog računarstva (preuzeto sa `Wikipedijine stranice o paralelnom računanju <http://en.wikipedia.org/wiki/Parallel_computing>`__):

* gusta i rijetka linearna algebra,
* spektralne metode (kao što je Cooley-Tukey brza Fourierova transformacija),
* problemi međudjelovanja čestica (kao što je Barnes-Hutova simulacija), 
* problemi kvadratične rešetke (kao što su Boltzmannove metode rešetke),
* problemi poligonalna rešetke (kao što se nalaze u analizi konačnih elemenata),
* Monte Carlo simulacije,
* kombinatorna logika (kao što su brute-force kriptografske tehnike),
* obilazak grafa (kao što su algoritmi pretraživanja),
* dinamičko programiranje,
* metode grananja i ograničavanja,
* multi-start metaheuristike,
* grafički modeli (kao što je traženje skrivenih Markovljevih modela i konstrukcija Bayesovih mreža),
* simulacije konačnih automata.

Razvoj raralelnih računala
--------------------------

Evolucija računalnih sustava prema `AMD <http://en.wikipedia.org/wiki/Advanced Micro Devices>`__-u uključuje tri odvojene ere (preuzeto iz prezentacije `AMD Fusion Fund Overview <http://www.slideshare.net/AMD/amd-fusion-fund-media-presentation>`__):

* **era jednojezgrenih sustava** traje otprilike do 2004.

  * **primjeri:** AMD Athlon XP i stariji, Intel Pentium 4 i stariji
  * **razvoj omogućuju:** `Mooreov zakon <http://en.wikipedia.org/wiki/Moore's_law>`__, povećanje napona, `Dennardova teorija smanjivanja MOSFET-a <http://en.wikipedia.org/wiki/MOSFET#MOSFET_scaling>`__
  * **razvoj ograničavaju:** potrošnja energije, složenost arhitekture
  * **programski alati:** `asembler <http://en.wikipedia.org/wiki/Assembly language>`__ -> `C <http://en.wikipedia.org/wiki/C_(programming_language)>`__ i `C++ <http://en.wikipedia.org/wiki/C++>`__ -> `Java <http://en.wikipedia.org/wiki/Java_(programming_language)>`__ i `Python <http://en.wikipedia.org/wiki/Python_(programming_language)>`__

* **era višejezgrenih sustava** traje otprilike do 2011.

  * **primjeri:** AMD Phenom serija, AMD FX serija, Intel Core serija
  * **razvoj omogućuju:** paralelizacija softvera, Mooreov zakon, `SMP <http://en.wikipedia.org/wiki/Symmetric_multiprocessing>`__ arhitektura
  * **razvoj ograničavaju:** potošnja energije, (ne)paralelnost softvera, skalabilnost
  * **programski alati:** `pthreads <http://en.wikipedia.org/wiki/POSIX_Threads>`__ -> `OpenMP <http://en.wikipedia.org/wiki/OpenMP>`__ -> `MPI <http://en.wikipedia.org/wiki/Message Passing Interface>`__

* **era heterogenih sustava** je `trenutno <http://developer.amd.com/tools-and-sdks/opencl-zone/>`__  `hit <http://cs264.org/>`__ `tema <https://code.google.com/p/stanford-cs193g-sp2010/>`__ u `akademskom <http://research.nvidia.com/content/cuda-courses-map>`__ `istraživačkom <https://research.nvidia.com/>`__ i `nastavnom <https://www.coursera.org/course/hetero>`__ `okruženju <http://www.fsb.unizg.hr/mat-4/?PARALELNI_ALGORITMI>`__

  * **primjeri:** AMD Llano i Trinity (A, E i C serije), Intel Sandy Bridge i Ivy Bridge (Core i3/i5/i7-2000 i 3000 serije), NVIDIA Tegra
  * **razvoj omogućuju:** `programabilni Shaderi <http://en.wikipedia.org/wiki/Shader_(realtime,_logical)>`__, masivna paralelizacija softvera, energetski efikasni GPU-i, `GPGPU <http://en.wikipedia.org/wiki/GPGPU>`__
  * **razvoj ograničavaju:** načini programiranja, pretek zbog komunikacije
  * **programski alati:** `NVIDIA Cg <http://en.wikipedia.org/wiki/Cg_(programming_language)>`__, `Microsoft HLSL <http://en.wikipedia.org/wiki/High_Level_Shader_Language>`__ -> `NVIDIA CUDA <http://en.wikipedia.org/wiki/CUDA>`__, `Microsoft DirectCompute <http://en.wikipedia.org/wiki/DirectCompute>`__ -> `OpenCL <http://en.wikipedia.org/wiki/OpenCL>`__

Distribuirano računarstvo
-------------------------

Arhitektura distribuiranih sustava
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. todo::

  Ovaj dio treba napisati.

Standard Message Passing Interface (MPI)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* sučelje za paralelizaciju aplikacija zasnovano na razmjeni poruka (engl. *message passing*)
* procesi komuniciraju putem cijevi, nema dijeljene memorije

  * jednostavno raspodijeliti procese za izvođenje na više računala
  * **donekle** sličan način rada kao modul ``multiprocessing`` u Pythonu

* aplikacija se pokreće korištenjem pomoćnih alata kao MPI **posao** (engl. *MPI job*)

  * svaki posao se sastoji od više procesa na jednom ili više računala
  * kod većih sustava alati kao `HTCondor <http://research.cs.wisc.edu/htcondor/>`__ služe za redanje više MPI poslova za izvođenje

* otvoreni standard, specifikacija dostupna na `MPI Forumu <http://www.mpi-forum.org/>`__

  * zadnja verzija standarda je MPI-3
  * najkorištenije značajke dio (koje ćemo i mi koristiti) su dio i MPI-1 verzije standarda; novosti iz MPI-2 se nešto rjeđe koriste

* podržan u mnogim jezicima: C, C++ (`Boost.MPI <http://www.boost.org/libs/mpi/>`__), Fortran, Java (`MPJ <http://mpj-express.org/>`__), Python, Perl, Ruby, ...
* dvije implementacije se aktivno razvijaju; podrška za MPI-2 je postoji već dugo vremena, podrška za MPI-3 je dostupna odnedavno

  * `Open MPI <http://www.open-mpi.org/>`__ (najpopularnija implementacija, nasljednik LAM/MPI)
  * `MPICH2 i MPICH 3 <http://www.mpich.org/>`__ (također vrlo popularna implementacija, nasljednik MPICH)
  * velika prednost: standardizirano sučelje => **kompatibilnost na razini izvornog koda**

* dvije implementacije se samo održavaju; kompletna podrška za MPI-1, djelomična podrška za MPI-2

  * `LAM/MPI <http://www.lam-mpi.org/>`__
  * `MPICH <http://www.mpich.org/>`__

* korišten u znanosti i istraživanju, dostupno puno tutoriala

  * `Lawrence Livermore National Laboratory tutorial <https://computing.llnl.gov/tutorials/mpi/>`__
  * `Stanford Linear Acceleration Center tutorial <http://www.slac.stanford.edu/comp/unix/farm/mpi.html>`__
  * `Google pretraga za "MPI tutorial" daje još mnogo rezultata... <https://www.google.com/search?q=mpi+tutorial>`__

Heterogeno računarstvo
----------------------

Iako AMD nije izmislio termine **heterogeno računarstvo** (engl. heterogeneous computing) i **heterogena sustavska arhitektura** (engl. heterogeneous system architecture, HSA), na svojim stranicama opisuje oba pojma:

* `Što je heterogeno računarstvo? <http://developer.amd.com/resources/heterogeneous-computing/what-is-heterogeneous-computing/>`__
* `Što je heterogena sustavska arhitektura? <http://developer.amd.com/resources/heterogeneous-computing/what-is-heterogeneous-system-architecture-hsa/>`__

Arhitektura heterogenih sustava
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Računalo s kojim ćemo raditi ima dva dijela:

* domaćin (engl. *host*), u našem slučaju CPU
* uređaj (engl. *device*), u našem slučaju GPU

U budućnosti se očekuje hardver i s tim programske paradigme kod kojih će memorija domaćina i uređaja biti dijeljena i način programiranja će zbog toga biti biti nešto pojednostavljen, ali svi koncepti koje u nastavku opisujemo, kao i način razmišljanja koji koristimo, i dalje će vrijediti.

Tehnologije NVIDIA Compute Unified Device Architecture (CUDA) i OpenCL
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. todo::

  Ovaj dio treba napisati.

Sadašnjost i budućnost heterogenih sustava
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Pored toga, AMD opisuje razvoje heterogene arhitekture sustava kroz 4 etape (preuzeto iz AMD-ove prezentacije `HSA Overview <http://www.slideshare.net/hsafoundation/hsa-overview>`__):

* **fizička integracija** (2011. godine, Llano) -- CPU i GPU nalaze se na jednom siliciju; CPU i GPU dijele jedinicu za upravljanje memorijom
* **optimizacija platforme** (2012. godina, Trinity i `Richland <http://youtu.be/MQcjEA3it90>`__) -- CPU i GPU dijele cjelokupnu količinu memorije, GPU može alocirati koliko god je potrebno; CPU i GPU imaju zajedničko dinamičko upravljanje energijom
* **arhitekturalna integracija** (2013. godina, Kaveri) -- CPU i GPU vide unificirani memorijski prostor, pokazivači se mogu prosljeđivati u oba smjera; GPU može pristupati CPU međuspremnicima
* **sustavska integracija** (2014. godina) -- GPU multitasking, specifično mogućnost da se izvede context switch između grafičkih i compute aplikacija; GPU pre-emption, specifično mogućnost da se zaustavi proces koji se dugo izvodi radi procesa koji će se izvoditi kraće, prioriteti izvođenja aplikacija

Komercijalni čip koji sadrži CPU i GPU na jednom siliciju, zasnovan na heterogenoj sustavskoj arhitekturi, AMD naziva `APU <http://en.wikipedia.org/wiki/AMD_Accelerated_Processing_Unit>`__. Intel i NVIDIA, unatoč tome što imaju vrlo slične čipove, zasad ovaj termin nisu prihvatili.
