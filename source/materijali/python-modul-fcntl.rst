.. sectionauthor:: `Vedran Miletić <http://vedranmileti.ch/>`__

Python: usluge specifične za operacijske sustave slične Unixu: pozivi fcntl() i ioctl()
=======================================================================================

* ``module fcntl`` (`službena dokumentacija <http://docs.python.org/py3k/library/fcntl.html>`__) nudi pristup pozivima ``fcntl()`` i ``ioctl()``
* ``fcntl.fcntl()``
* ``fcntl.flock()``
* ``fcntl.ioctl()``
* ``fcntl.lockf()``

.. todo::

  Ovaj dio treba napisati u cijelosti.
