.. sectionauthor:: `Vedran Miletić <http://vedranmileti.ch/>`__

Python: internacionalizacija i lokalizacija
===========================================

* ``module locale`` (`službena dokumentacija <http://docs.python.org/py3k/library/locale.html>`__) nudi pristup lokalnim i regionalnim postavkama operacijskog sustava

.. todo::

  Ovaj dio bi trebalo napisati u cijelosti.
