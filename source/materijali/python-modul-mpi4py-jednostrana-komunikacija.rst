.. sectionauthor:: `Vedran Miletić <http://vedranmileti.ch/>`__

Rad s Python modulom mpi4py: jednostrana komunikacija
=====================================================

Dokumentacija svih funkcija dana je u sklopu opisa `aplikacijskog programskog sučelja klase Win <http://mpi4py.scipy.org/docs/apiref/mpi4py.MPI.Win-class.html>`__.
