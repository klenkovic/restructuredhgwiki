.. sectionauthor:: `Vedran Miletić <http://vedranmileti.ch/>`__

Rad s Python modulom mpi4py: paralelni ulaz/izlaz
=================================================

Dokumentacija svih funkcija dana je u sklopu opisa `aplikacijskog programskog sučelja klase File <http://mpi4py.scipy.org/docs/apiref/mpi4py.MPI.File-class.html>`__.

.. todo::

  Ovaj dio treba napisati u cijelosti.
