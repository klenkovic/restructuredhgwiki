.. sectionauthor:: `Vedran Miletić <http://vedranmileti.ch/>`__

Rad s Python modulom mpi4py: upravljanje okolinom
=================================================

Inicijalizacija i finalizacija
------------------------------

Dokumentacija svih funkcija dana je u sklopu opisa `aplikacijskog programskog sučelja modula MPI <http://mpi4py.scipy.org/docs/apiref/mpi4py.MPI-module.html>`__.

* ``Init()`` -- incijalizacija okoline za izvođenje MPI aplikacije
* ``Finalize()`` -- uništavanje okoline izvođenja MPI aplikacije

Informacije o implementaciji
----------------------------

* ``Get_version()`` -- vraća verziju MPI standarda koja se koristi kao uređeni par oblika ``(major, minor)``
* ``Get_library_version()`` -- vraća verziju MPI standarda koja se koristi kao niz znakova
* ``Get_processor_name()`` -- vraća ime domaćina koji izvodi proces

.. todo::

  Ovdje nedostaje zadatak.

Mjerači vremena
---------------

* ``Wtime()`` -- vraća vrijeme operacijskog sustava kao vrijednost tipa ``float``, ekvivalent funkciji ``time()`` unutar Python modula ``time``
* ``Wtick()`` -- vraća informacije o preciznosti mjerača vremena

.. todo::

  Ovdje nedostaje zadatak sa mjerenjem vremena izvođenja nekog algoritma.

Baratanje pogreškama
--------------------

Baratanje pogreškama izvodi se hvatanjem iznimki. Pozivi MPI funkcija u slučaju greške podižu iznimku, instancu klase ``Exception``. Dokumentacija svih funkcija dana je u sklopu opisa `aplikacijskog programskog sučelja klase Exception <http://mpi4py.scipy.org/docs/apiref/mpi4py.MPI.Exception-class.html>`__.
