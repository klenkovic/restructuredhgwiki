.. sectionauthor:: `Vedran Miletić <http://vedranmileti.ch/>`__

Python: općenite usluge operacijskog sustava: osnovna sučelja
=============================================================

* ``module os`` (`službena dokumentacija <http://docs.python.org/py3k/library/os.html>`__) pruža prenosiv način za pristup sučeljima operacijskog sustava

Dohvaćanje informacija o procesu i operacijskom sustavu
-------------------------------------------------------

* ``os.uname()`` vraća ime operacijskog sustava
* ``os.getlogin()`` vraća korisničko ime korisnika kojem pripada proces
* ``os.kill(pid, sig)`` šalje signal ``sig`` procesu s PID-om ``pid``
* ``os.nice(increment)`` povećava niceness procesa
* ``os.getpid()`` vraća PID procesa interpretera
* ``os.getppid()`` vraća PPID procesa interpretera

Efektivni UID i GID utječu na dozvole za stvaranje i pristup datotekama.

* ``os.geteuid()``
* ``os.seteuid(euid)``
* ``os.getegid()``
* ``os.setegid(egid)``

Stvarni UID i GID utječu na dozvole za slanje signala procesima.

* ``os.getuid()``
* ``os.setuid(uid)``
* ``os.getgid()``
* ``os.setgid(gid)``
* ``os.getgroups()``
* ``os.setgroups(groups)``

* ``os.umask(mask)`` postavlja korisničku masku na vrijednost ````mask````
* ``os.getcwd()`` vraća trenutni radni direktorij procesa
* ``os.chdir(path)`` mijenja trenutni radni direktorij procesa u ````path````
* ``os.listdir(path)`` izlistava sadržaj direktorija, kao rezultat vraća listu znakovnih nizova koji su imena datoteka i poddirektorija

.. admonition:: Zadatak

  Napišite program koji kod pokretanja ispisuje iduće informacije:

  * vlastiti PID, PID roditelja,
  * efektivni UID i GID,
  * stvarni UID i GID,
  * direktorij u kojem radi i njegov sadržaj,
  * ime terminala kojem pripada. (**Uputa:** Potražite funkciju u dokumentaciji modula ``os`` i iskoristite 0, 1 ili 2 za broj opisnika datoteke. Usporedite tu vrijednost s izlazom naredbe ``tty``.)

.. admonition:: Zadatak

  Na temelju:

  * stvarnog UID-a i sadržaja datoteke ``/etc/passwd``, te
  * stvarnog GID-a i sadržaja datoteke ``/etc/group``,

  saznajte ime korisnika i grupe. Postoji li elegantniji način za to? (**Uputa:** Potražite funkciju u dokumentaciji modula ``os``.)

Pokretanje procesa i podprocesa
-------------------------------

* ``os.environ`` je rječnik varijabli okoline
* ``os.exec{l,v{,p,e,pe}}()`` pokreće proces koji zamjenjuje trenutni proces

  * novi proces zadržava PID i okolinu
  * stog, hrpa i podaci procesa zamjenjuju se novim
  * **l** -- argumenti naredbe navedeni su kao argumenti funkcije ``execl()``
  * **v** -- argumenti naredbe navedeni su kao lista koja je jedan arugment funkcije ``execv()``
  * **p** -- iskoristi varijablu okoline ``$PATH`` za pronalaženje datoteke za pokretanje
  * **e** -- kod pokretanja naredbe prosljeđuje se i rječnik varijabli okoline koje se dodaju ``os.environ``

* ``os.execl()`` ima četiri varijante:

  * ``os.execl(path, arg0, arg1, ...)``
  * ``os.execle(path, arg0, arg1, ..., env)``
  * ``os.execlp(file, arg0, arg1, ...)``
  * ``os.execlpe(file, arg0, arg1, ..., env)``

* ``os.execv()`` također ima četiri varijante:

  * ``os.execv(path, args)``
  * ``os.execve(path, args, env)``
  * ``os.execvp(file, args)``
  * ``os.execvpe(file, args, env)``

.. admonition:: Zadatak

  * Napišite Python skriptu koja pokreće ``ls`` na direktorij ``/etc`` tako da iskoristite varijatnu funkcije ``exec()`` koja za traženje naredbe koristi varijablu okoline ``$PATH``.
  * Modificirajte skriptu tako da umjesto ``ls`` pokrećete ``cal`` i da se kod pokretanja okolina modificira tako da ``$LANG`` poprimi vrijednost ``en_US.UTF-8``.

* zombie proces, ponekad nazvan mrtav proces (engl. *defunct process*), proces koji je završio s izvođenjem ali i dalje ima unos u tablici procesa zbog potrebe da njegov roditelj pročita izlazni status

* ``os.fork()`` stvara kopiju procesa; procesu roditelju vraća PID procesa djeteta, procesu djetetu vraća 0 (na temelju toga ih razlikujemo)
* ``os.forkpty()``
* ``os.wait()`` čeka na završetak izvođenja procesa djeteta; vraća uređeni par ``(pid, status)``
* ``os.waitpid()``

.. admonition:: Zadatak

  Napišite Python skriptu koja radi ``fork()`` i u procesu djetetu pokreće proces koji izlistava trenutni direktorij.

  * Na koji ćete način osigurati da se naredba pokreće samo u procesu djetetu?
  * Treba li vam ovdje ``wait()``? Objasnite zašto.

  (Vremenom ćemo naučiti koristiti elegantnije sučelje za baratanje potprocesima, u sklopu modula ``subprocess``.)

* grupa procesa je skup koji se sastoji od jednog ili više procesa

  * identifikator grupe procesa (engl. *Process Group Identifier*, PGID) jednak je PID-u procesa koji je voditelj grupe

* sesija je skup koji se sastoji od jedne ili više procesnih grupa

  * postoji definiran proces koji je voditelj sesije
  * procesi jedne sesije mogu stvarati procesne grupe samo unutar te sesije

* ``os.getsid(pid)`` vraća id sesije procesa s PID-om ``pid``
* ``os.setsid()`` stvara novu sesiju u kojoj trenutni proces postaje voditelj i sesije i vlastite grupe

* upravljački terminal (engl. *controlling terminal*, CTTY) je terminal koji može biti pridružen sesiji

  * odlučuje o stvarima kao što su koji proces prima ulaz s tipkovnice i kako procesi primaju informacije o promjeni veličine prozora terminala
  * `upravljački terminal se može mijenjati <http://blog.nelhage.com/2011/02/changing-ctty/>`__
  * sesija koja nema upravljački terminal dobiva ga `kad voditelj otvori prvu datoteku <http://blog.nelhage.com/2011/01/reptyr-attach-a-running-process-to-a-new-terminal/>`__

* `daemon proces <http://en.wikipedia.org/wiki/Daemon_(computer_software)>`__ je proces pokrenut u pozadini, bez terminala koji njime upravlja, često bez roditelja (``init`` ga "posvaja")

  * primjeri: ``sshd``, ``httpd``, ``syslogd``, ``ntpd``, ``acpid``, ...

.. admonition:: Zadatak

  Napišite program koji izvodi takozvani `daemon fork <http://code.activestate.com/recipes/66012-fork-a-daemon-process-on-unix/>`__, koji koristi takozvani *double fork magic*:

  * prvi ``fork()`` služi za stvaranje potprocesa koji se zatim odvaja od upravljački terminala i roditelja tako što postaje voditelj sesije i vlastite procesne grupe, mijenja radni direktorij u ``/`` i podešava korisničku masku na 0,
  * drugi ``fork()`` osigurava da proces prestaje biti voditelj sesije, kako ne bi pridobio upravljački terminal.

  Nakon svakog ``fork()``a izvedite ``exit()`` procesa roditelja. To se, gledano iz perspektive procesa roditelja, često popularno naziva *fork off and die*.

.. admonition:: Zadatak

  Modificirajte kod koji radi daemon fork tako da proces koji je demon čini sljedeće:

  * otvara datoteku ``demon.txt`` za zapisivanje,
  * saznaje ID korisnika i ispisuje ga na ekran, a zatim u datoteku,
  * saznaje ime operacijskog sustava i ispisuje ga na ekran, a zatim u datoteku,
  * spava 5 sekundi,
  * ispisuje na ekran trenutni datum i vrijeme, a zatim u datoteku,
  * ispisuje na ekran vrijednost varijable okoline ``$PATH`` (sintaksa/način rada: ``os.environ['IME_VARIJABLE']``),
  * uspisuje u datoteku niz znakova "KRAJ", i zatim zatvara datoteku.
