.. sectionauthor:: `Vedran Miletić <http://vedranmileti.ch/>`__

Python: dodatne usluge operacijskog sustava: poziv select()
===========================================================

* ``module select`` (`službena dokumentacija <http://docs.python.org/py3k/library/select.html>`__) omogućuje pristup pozivima ``select()`` i ``poll()``
* ``select.select(rlist, wlist, xlist[, timeout])`` čeka do trenutka kad su jedan ili više opisnika datoteke navedeni u listama ``rlist``, ``wlist`` i ``xlist`` spremni za neku od U/I operacija (čitanje, zapisivanje, iznimka)

  * liste specijalno mogu biti i prazne
  * vraća uređenu trojku ``(rlist, wlist, xlist)`` gdje je svaki od elemenata ''podlista'' pripadne liste u pozivu
  * ``timeout`` specificira maksimalno vrijeme čekanja u sekundama; ukoliko nije naveden ili je ``None``, poziv će čekati unedogled

* ``select.poll()`` vraća objekt koji na koji se mogu registrirati opisnici datoteka, a zatim se može koristiti za ispitivanje spremnosti na određenu operaciju

.. admonition:: Zadatak

  Prisjetimo se da ``mkfifo`` stvara imenovane cijevi.

  * Stvorite imenovanu cijev ``fifo000`` i zapisujte u nju ``cat``-om proizvoljan sadržaj.
  * Otvorite istu imenovanu cijev kao datoteku za čitanje u Pythonu. Možete li koristiti ``read()``, ``readline()`` i ``readlines()``? Objasnite zašto.
  * Napišite kod koji koristi ``select()`` da otkrije kada može čitati sadržaj datotke. Što se dogodi kada prekinete ``cat``?
