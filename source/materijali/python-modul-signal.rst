.. sectionauthor:: `Vedran Miletić <http://vedranmileti.ch/>`__

Python: međuprocesna komunikacija: baratanje signalima
======================================================

* ``module signal`` (`službena dokumentacija <http://docs.python.org/py3k/library/signal.html>`__) 

.. todo::

  Ovaj dio bi trebalo napisati u cijelosti.
