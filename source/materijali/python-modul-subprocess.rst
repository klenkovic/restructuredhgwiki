.. sectionauthor:: `Vedran Miletić <http://vedranmileti.ch/>`__

Python: međuprocesna komunikacija: podprocesi
=============================================

* ``module subprocess`` (`službena dokumentacija <http://docs.python.org/py3k/library/subprocess.html>`__) omogućuje pokretanje novih (pod)procesa, povezivanje na njihove cijevi za ulaz, izlaz i greške, i dohvaćanje njihovih izlaznih kodova
* ``subprocess.call([arg0, arg1, ...])`` poziva naredbu s navedenim argumentima i vraća njezin izlazni status
* ``subprocess.getoutput("arg0 arg1 ...")`` poziva naredbu s navedenim argumentima i vraća njezin izlaz
* ``subprocess.getstatusoutput("arg0 arg1 ...")`` poziva naredbu s navedenim argumentima i vraća uređeni par ``(status, output)``

.. code-block:: python

  subprocess.getoutput("ls -l")
  subprocess.getstatusoutput("ls -l")
  # za usporedbu
  subprocess.call(["ls", "-l"])

* uočimo razliku u navođenju naredbe i parametara u obliku liste kod ``subprocess.call()`` nasuprot navođenju naredbe i parametara u obliku znakovnog niza kod ``subprocess.getoutput()`` i ``subprocess.getstatusoutput()``

.. admonition:: Zadatak

  * Usporedite izlaz funkcije ``os.listdir()`` s izlazom naredbe ``ls`` dohvaćenim korištenjem sučelja koja nudi modul ``subprocess``. Ima li razlike u datotekama koje su izlistane?
  * Izvedite čitanje sadržaja datoteke ``/etc/group`` korištenjem standardnih Python sučelja za rad sa datotekama, a zatim dohvaćanjem izlaza naredbe ``cat`` na tu datoteku. Ima li razlike u pročitanom sadržaju?

* ``subprocess.Popen()`` vraća objekt pridružen potprocesu koji pokreće, a zatim se može koristiti za upravljanje tim procesom

  * ``subprocess.Popen.communicate()`` prima ono što se prosljeđuje na ``stdin`` i to mora biti tipa ``bytes``, a vraća uređeni par ``(stdout, stderr)``, oba elementa su tipa ``bytes``

* ``subprocess.PIPE`` je objekt koji se koristi da se standardni ulaz, standardni izlaz ili standardni izlaz za greške dohvate u Python interpreteru korištenjem metode ``communicate()`` objekta pridruženog procesu

.. code-block:: python

  a = subprocess.Popen(["ls", "-l"], stdout=subprocess.PIPE)
  a.communicate()

  b = subprocess.Popen(["rm", "-i", "dat3"], stdin=subprocess.PIPE)
  b.communicate("y\n".encode())

.. admonition:: Zadatak

  Korištenjem objekta subprocess.Popen pokupite izlaz naredbe man koji daje kada je pokrećete bez argumenata. (**Uputa:** biti će ispisan na standardni izlaz za greške.)

.. admonition:: Zadatak

  Napravite Python skriptu koja traži od korisnika imena triju datoteka. Datoteke se brišu interaktivnim načinom rada, a od korisnika se traži unos na standardni ulaz "kao da izravno radi s naredbom" (odnosno unosi ``y`` ako želi obrisati i ``n`` ako ne želi, a to se prosljeđuje naredbi putem metode ``communicate()`` objekta pridruženog procesu).

.. admonition:: Zadatak

  Za stvaranje korisnika na Linuxu koristi se naredba ``useradd``. Proučite pripadnu man stranicu, a zatim napišite Python skriptu koja, uz pretpostavku da imate odgovarajuće ovlasti:

  * stvara korisnike ``student01``, ``student02``, ..., ``student99`` s pripadnim kućnim direktorijima,
  * u kućnom direktoriju svakog od korisnika stvara direktorij ``python-samples`` kojem postavlja dozvole na ``rwxrwxrwx``,
  * na kraj datoteke ``.bashrc`` dodaje naredbu koja podešava korisničku masku na 077.
