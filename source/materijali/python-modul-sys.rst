.. sectionauthor:: `Vedran Miletić <http://vedranmileti.ch/>`__

Python: parametri i funkcije ovisni o sustavu
=============================================

* ``module sys`` (`službena dokumentacija <http://docs.python.org/py3k/library/sys.html>`__) nudi pristup objektima koje interpreter koristi i s kojima komunicira

.. admonition:: Zadatak

  Pokrenite Python 3 interpreter i učitajte modul ``sys``.

  * Otkrijte kojeg su tipa objekti ``sys.ps1`` i ``sys.ps2``.
  * Promijenite im vrijednost na drugu vrijednost po vašem izboru. Što uočavate? Fora, a? ;-)

* ``sys.version`` je verzija Pythona i program prevoditelja kojim je preveden
* ``sys.platform`` je platforma (operacijski sustav)
* ``sys.modules`` je rječnik učitanih modula
* ``sys.getdefaultencoding()`` vraća najčešće ``'utf-8'``
* ``sys.getfilesystemencoding()`` vraća najčešće ``'utf-8'``
* ``sys.getsizeof(object)`` vraća veličinu objekta ``object`` u bajtovima
* ``sys.int_info`` daje informacije o načinu spremanja cijelih brojeva
* ``sys.maxsize`` daje informacije o maksimalnoj duljini liste, tuplea, skupa ili rječnika
* ``sys.exit([status])`` izlazi iz interpretera s povratnim kodom ``status``

  * raspon povratnih kodova (izlaznih statusa) je 0--127 i 129--192; postoje pokušaji standardizacije značenja svakog pojedinog broja, primjerice ``sysexits.h``
  * u ljusci ``bash`` varijabla ljuske ``$?`` sadrži izlazni status

* ``sys.argv`` je lista argumenata proslijeđenih Python skripti kod pokretanja

  * ``argv[0]`` je ime Python skripte
  * ``argv[i]`` je $i$-ti argument naveden nakon imena skripte

.. admonition:: Zadatak

  Napišite Python 3 skriptu imena ``zadatak-argv.py`` koja će nakon što je iz komandne linije pozovemo s argumentom ``arg1`` (``./zadatak-argv.py arg1`` ili ``python3.2 zadatak-argv.py arg1``) ispisati:

  * ``Pozvali ste skriptu imena zadatak-argv.py s argumentom arg1``, pri čemu su ``zadatak-argv.py`` i ``arg1`` vrijednosti varijabli.
  * Ako je prvi argument jednak ``pythonversion``, onda će taj program pored toga ispisati verziju Python interpretera kojeg koristimo, u našem slučaju '3.2.2'.
  * Ako je prvi argument jednak ``gccversion``, onda će taj program ispisati verzi GCC-a kojom je korišteni Python interpreter preveden, u našem slučaju '4.2.1'.

  (**Uputa:** razmislite kako ćete iz ``sys.version`` izvući vrijednosti koje vam trebaju.)

.. admonition:: Zadatak

  Modificirajte program iz prethodnog zadatka tako da:

  * U slučaju da korisnik unese nedozvoljnu vrijednost prvog argumenta skripta na standardni izlaz za greške ispisuje poruku o tome, a zatim izlazi s izlaznim statusom 1.
  * U slučaju da korisnik unese manje ili više argumenata skripta na standardni izlaz za greške ispisuje poruku o tome, a zatim izlazi s izlaznim statusom 2.

  (**Uputa:** Pogledajte u pomoći za funkciju ``print()`` kako ispisati sadržaj na standardni izlaz za greške).

* opisnik datoteke (engl. *file descriptor*) je cjelobrojni indeks u tablici otvorenih datoteka vezanih uz proces
* svaki proces ima barem 3 opisnika: standardni ulaz, standardni izlaz i standardni izlaz za greške

  * objekt ``sys.stdin`` reprezentira standardni ulaz (opisnik 0)
  * objekt ``sys.stdout`` reprezentira standardni izlaz (opisnik 1)
  * objekt ``sys.stderr`` reprezentira standardni izlaz za greške (opisnik 2)

* naredba ``lsof`` ispisuje popis otvorenih datoteka na sustavu

  * ``lsof -p pid`` ispisuje popis datoteka koje je otvorio proces s PID-om ``pid``

.. admonition:: Zadatak

  * Isprobajte ``sys.stdout.write`` i objasnite na koji je način rahličit od ``print()``. Prima li više od jednog argumenta? Ispisuje li novi redak? Možete li ispisati cijeli ili realan broj?
  * Prijavite se u dva terminala ili alternativno backgroundajte Python interpreter nakon što ste u njemu otvorili proizvoljnu datoteku iz vašeg direktorija. Pronađite u popisu otvorenih datoteka vaš proces, 3 osnovna opisnika i opisnik te datoteke.
