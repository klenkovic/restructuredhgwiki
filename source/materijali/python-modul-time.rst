.. sectionauthor:: `Vedran Miletić <http://vedranmileti.ch/>`__

Python: općenite usluge operacijskog sustava: vrijeme
=====================================================

* ``module time`` (`službena dokumentacija <http://docs.python.org/py3k/library/time.html>`__) nudi funkcije vezane uz vrijeme
* ``time.time()`` vraća trenutno vrijeme sustava, ``float`` zapis
* ``time.ctime()`` vraća trenutno vrijeme sustava, ``str`` zapis
* ``time.sleep(seconds)`` čini da proces *spava* ``seconds`` sekundi

.. admonition:: Zadatak

  Napišite program koji na početku i na kraju zapisuje vrijeme u dvije različite varijable (recimo, start_time i end_time). (Cilj je mjeriti vrijeme izvođenja.)

  Između te dva pridruživanja vrijednosti učinite da program "spava" 20 sekundi, nakon toga ispiše na ekran ``"spavao sam 20 sekundi"``.

  Nakon računanja ukupnog vremena izvođenja kao razlike krajnjeg i početnog vremena, program ispisuje na ekran ``"Ukupno vrijeme izvođenja: <vrijeme>"``. (**Napomena:** Razmislite paše li vam više float zapis ili str zapis.)

.. admonition:: Zadatak

  Izmjerite brzinu izvođenja ovih operacija u Pythonu:

  * računanje produkta prvih 100 000 prirodnih brojeva,
  * računanje zbroja prvih 500 000 prirodnih brojeva.
