.. sectionauthor:: `Vedran Miletić <http://vedranmileti.ch/>`__

Python: međuprocesna komunikacija: pozivanje udaljenih procedoura i modul XML-RPC
=================================================================================

* ``module xmlrpc.client`` (`službena dokumentacija <http://docs.python.org/3/library/xmlrpc.client.html|službena dokumentacija>`__) nudi funkcije klijentske strane
* ``module xmlrpc.server`` (`službena dokumentacija <http://docs.python.org/3/library/xmlrpc.server.html>`__) nudi funkcije poslužiteljske strane

.. todo::

  Ovaj dio treba napisati u cijelosti.
