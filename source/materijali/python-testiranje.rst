.. sectionauthor:: `Vedran Miletić <http://vedranmileti.ch/>`__

Testiranje Python aplikacija
============================

.. note::

  Način koji ćemo mi koristiti je dovoljan za naše potrebe. Kod većih aplikacija koje se izrađuju za dugotrajniju primjenu u stvarnom svijetu bolje je koristiti Python biblioteku ``unittest`` (`službena dokumentacija <http://docs.python.org/3/library/unittest>`__).

Imamo li datoteku ``modul1.py`` sadržaja

.. code-block:: python

  # -*- coding: utf-8 -*-
  def funkcija1():
      """
      Funkcija uvijek vraća vrijednost 5.

      Vraća:
      Vrijednost 5, tipa int.
      """
      return 5

Jednostavan test možemo izvesti korištenjem naredbe ``assert``. stvorimo datoteku ``test_program.py`` sadržaja:

.. code-block:: python

  import modul1

  def test_funkcija1():
      assert modul1.funkcija() == 5
      print("Test funkcije funkcija1 uspješno prolazi.")

  test_funkcija1()

Pokretanjem ovog programa vrši se testiranje modula ``modul1``. Navedeni kod može se pojednostaviti korištenjem modula `pytest <http://pytest.org/>`__ (naredba ``py.test``) na način:

.. code-block:: python

  import modul1

  def test_funkcija():
      assert modul1.funkcija() == 5

Uočimo kako smo eliminirali korištenje funkcije ``print()`` za ispis rezultata testa i poziv funkcije. Kod većeg broja testova ovo pojednostavljenje je značajno.

Pokretanjem naredbe ``py.test-3`` u direktoriju u kojem se ova datoteka nalazi vrši se pokretanje svih funkcija čije ime počinje sa ``test_`` unutar svih datoteka čije ime počinje sa ``test_``.

.. note::

  Naredba ``pytest`` nije povezana s alatom pytest, unatoč tome što im je ime vrlo slično. Navedena naredba dio je biblioteke `logilab-common <http://www.logilab.org/project/logilab-common>`__.

.. admonition:: Zadatak

  * Promijenite konstantu ``5`` u testu u neku drugu konstantu. Prolazi li test?
  * Definirajte funkciju koja prima broj i vraća kvadrat tog broja. Definirajte test za nju s ulaznim podacima ``3``, ``15``, ``24``.
