.. sectionauthor:: `Vedran Miletić <http://vedranmileti.ch/>`__

Upravljanje paketima
====================

.. hint::

  Za više informacija proučite `Part II. Package Management <https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/System_Administrators_Guide/part-Package_Management.html>`__ u `Red Hat Enterprise Linux 7 System Administrator's Guide <https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/System_Administrators_Guide/>`__.

* softverski paket (engl. *software package*)

  * kraće *paket*; arhiva koja sadrži datoteke aplikacije, biblioteke ili dokumentacije (npr. ``firefox``, ``firefox-branding``, ``firefox-gnome-support``)
  * najpoznatiji ``.rpm`` i ``.deb``

* međuovisnost

  * međuovisnost kod prevođenja (engl. *compile-time dependancy*), programi i/ili bibliotečne datoteke koje program zahtjeva za uspješno prevođenje (npr. ``flex``, ``bison`` ili ``iostream``)
  * međuovisnost kod izvršavanja (engl. *run-time dependancy*), programi i/ili bibliotečne datoteke koje program zahtjeva dok radi

Osnove rada s upraviteljem paketa niže razine
---------------------------------------------

* upravitelj paketima niže razine

  * koristi se za stvaranje, instalaciju, deinstalaciju i konfiguriranje paketa
  * *"dependency hell"* (pakao međuovisnosti?)
  * najpoznatiji su `RPM Package Manager <http://en.wikipedia.org/wiki/RPM_Package_Manager>`__, naredba ``rpm``, i `dpkg <http://en.wikipedia.org/wiki/Dpkg>`__, naredba ``dpkg``

.. admonition:: Zadatak

  Proučite ``rpm(8)`` (notacija za ``man 8 rpm``).

  * Provjerite koji paketi koji u imenu sadrže niz znakova ``python`` su instalirani na sustavu.
  * Pronađite koje datoteke sadrži paket ``python-libs``.
  * Pronađite koji paket sadrži datoteku ``/bin/ls``, a koji datoteku ``/usr/bin/scp``. Objasnite što se dogodi ako izostavite putanju.

Osnove rada s upraviteljem paketa više razine
---------------------------------------------

* upravitelj paketima više razine

  * koristi se za pretraživanje, dohvaćanje i nadogradnju paketa, nalaženje paketa koji zadovoljavaju potrebne međuovisnosti
  * najpoznatiji su `Yellowdog Updater Modified <http://en.wikipedia.org/wiki/Yellowdog_Updater,_Modified>`__, kraće yum, nareba ``yum``, i `Advanced Packaging Tool <http://en.wikipedia.org/wiki/Advanced_Packaging_Tool>`__, kraće APT, naredbe ``apt-get``, ``apt-cache``, ...

* `repozitorij softvera <http://en.wikipedia.org/wiki/Software_repository>`__ (engl. *software repository*)

  * skup paketa i metapodataka o njima, najčešće udaljen i dostupan putem Interneta
  * sadrži metapodatke o paketima (naziv paketa, opis sadržaja, popis međuovisnosti, ...) i same pakete
  * repozitoriji softvera uključeni na sustavu navedeni su u ``/etc/yum.repos.d``

* popis paketa

  * doslovno popis paketa, dio repozitorija na poslužitelju, mijenja se sukladno promjenama u skupu paketa koji se s poslužitelja mogu preuzeti
  * upravitelj paketima više razine ima kopije jednog ili više popisa u lokalnom međuspremniku

.. admonition:: Zadatak

  * Pronađite u ``/etc`` koju datoteku yum koristi za konfiguraciju i pročitajte u njoj koji direktorij yum koristi za međuspremnik. Provjerite njegov trenutni sadržaj.
  * Ispišite popis paketa iz lokalnog međuspremnika naredbom ``yum list``. Prebrojite ih. Radi li se o instaliranim ili dostupnim paketima?
  * Provjerite ima li u popisu paketa paket ``python3``.
  * Pronađite informacije o paketu ``python``, specifično kolika mu je veličina i iz kojeg je repozitorija.

* ``yum check-update`` -- dohvaća nove popise paketa s poslužitelja i sprema ih u lokalni međuspremnik

* ``yum update`` -- dohvaća nove popise paketa s poslužitelja i sprema ih u lokalni međuspremnik, a zatim radi nadogradnju svih instaliranih paketa

* ``yum install paket`` -- instalira paket ukoliko on postoji u popisima

* ``yum remove/erase paket`` -- briše paket sa sustava zajedno sa svim paketima koji o njemu ovise, a konfiguracijske datoteke paketa sprema u ``.rpmsave`` u slučaju da su bile promijenjene od strane korisnika

* ``yum search paket`` -- traži paket u popisima

.. admonition:: Zadatak

  * Instalirajte paket ``nano``.
  * Izbrišite paket ``wireshark``.
  * Nadogradite sve pakete na sustavu. Hoće li ``yum`` automatski obnoviti popise?

Pakiranje paketa
----------------

.. todo::

  Ovaj dio treba napisati u cijelosti.

TODO za rasporediti
===================

..
  === Unix Toolbox ===
   * [[http://cb.vu/unixtoolbox.xhtml|Unix Toolbox]], vrlo dobra i koncizna dokumentacija najčešće korištenih naredbi

  == Upravljanje paketima. Upravljanje uslugama. Pokretanje i zaustavljanje poslužiteljskih procesa. Praćenje događaja i syslog. Konfiguracija mrežnih sučelja. Automatizacija sustavskih zadaća ==
  === Dokumentacija ===
   * [[https://access.redhat.com/knowledge/docs/Red_Hat_Enterprise_Linux/|Red Hat Enterprise Linux dokumentacija]]
    * Red Hat naplaćuje podršku za Enterprise Linux, sam softver je besplatan (i slobodan)
    * CentOS je Red Hat Enterprise Linux (RHEL) s drugačijim brandingom za koji se ne plaća podrška
    * CentOS 100% kompatibilan s originalom --> sva dokumentacija originala vrijedi
   * [[http://docs.fedoraproject.org/en-US/|Fedora dokumentacija]]
    * velikim dijelom odnosi se na RHEL/CentOS, ali ne 100%
    * RHEL 6 == CentOS 6 ~= Fedora 12, ali dobar dio dokumentacije vrijedi neovisno o verziji (u svakom slučaju možete eksperimentirati)
    * RHEL 7 je u razvoju, zasnovan na Fedori 19

  == Konfiguracija vatrozida: iptables. ==
  {{{#!wiki note
  **Napomena**

  Vatrozid iptables koji koristimo u nastavku najčešće se koristi modifikacijom poznatih naredbi, obzirom da je zbog velikog broja parametara dosta nezgrapno naredbe pisati "od nule". Primjeri gotovih naredbi koje ćemo koristiti kao predloške mogu se naći na [[http://www.thegeekstuff.com/2011/06/iptables-rules-examples/|25 Most Frequently Used Linux IPTables Rules Examples]].
  }}}

  {{{#!wiki important
  **Zadatak**

  Nađite način da na temelju "recepata" složite naredbu koja će blokirati ono što stiže sa 193.198.209.42 na port 80 na dani poslužitelj na kojem radite, ali da pritom ne blokira 193.198.209.200.
  }}}

   * kada inf2 radi ping na virtualni stroj, iz perspektive iptablesa na virtualnom stroju
    * ICMP ECHO REQUEST stiže sa inf2 na virtualni stroj u INPUT lanac
    * ICMP ECHO REPLY se šalje sa virtualnog stroja na inf2 kroz OUTPUT lanac

   * kada virtualni stroj radi ping na inf2, iz perspektive iptablesa na virtualnom stroju
    * ICMP ECHO REQUEST se šalje sa virtualnog stroja kroz OUTPUT lanac
    * ICMP ECHO REPLY stiže sa inf2 na virtualni stroj u INPUT lanac

  {{{#!wiki important
  **Zadatak**

  Blokirajte ping prema virtualnom stroju, ali ne i s njega.
  }}}

